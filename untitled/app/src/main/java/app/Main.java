package com.practice.app;


import com.practice.service.WeatherService;

import java.io.IOException;

import static com.practice.utils.PrintUtils.displayWeatherForecast;

public class Main {
    public static void main(String[] args) {
        WeatherService weatherService = new WeatherService();
        try {
            String weather = weatherService.getWeatherForecast();
            displayWeatherForecast(weather);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
